#include <stdio.h>
#include <math.h>
#include <time.h>
#include "sumArray.h"


int main(int argc , char* argv[]){

    int max_dim = (int) pow(2, 12);
    if(argc!=2){
        fprintf(stderr, "Usage %s <dimension>\n",argv[0]);
        return 1;
    }

    int n = atoi(argv[1]);
    if (n <= 0 || n > max_dim) {
        fprintf(stderr, "Error: dimension not between 1 and %d\n", max_dim);
        return 1;
    }
    double* A;
    A = (double*) malloc(n * n * sizeof(double));

  
    srand(time(NULL));
    for (int i = 0; i < n * n; i++) {
        A[i] = ((double) rand() / RAND_MAX) * 2.0 - 1.0;
    }
    
    // Matrix sum using sumArray1 and caculate time
    clock_t tic = clock();
    double sum1 = sumArray1(A, n);
    clock_t toc = clock();
    double time1 = (double) (toc - tic) / CLOCKS_PER_SEC * 1000.0;

    // Matrix sum using sumArray2 and caculate time
    tic = clock();
    double sum2 = sumArray2(A, n);
    toc = clock();
    double time2 = (double) (toc - tic) / CLOCKS_PER_SEC * 1000.0;


    printf(" %f\n %f\n %f\n %f\n", time1, sum1, time2, sum2);

  
    free(A);

   
    return 0;
}