#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "matmul.h"

#define n 1024

int main() {
    double *A, *B, *C1, *C2, *C3, *C4;
    clock_t tic, toc;
    double elapsed_time;

    // Allocate memory for matrices A, B, and C
    A = (double*) malloc(n*n*sizeof(double));
    B = (double*) malloc(n*n*sizeof(double));
    C1 = (double*) malloc(n*n*sizeof(double));
    C2 = (double*) malloc(n*n*sizeof(double));
    C3 = (double*) malloc(n*n*sizeof(double));
    C4 = (double*) malloc(n*n*sizeof(double));

    // Fill matrices A and B with random numbers in [-1,1]
    srand(time(NULL));
    for (int i = 0; i < n*n; i++) {
        A[i] = (double)rand() / (double)RAND_MAX * 2.0 - 1.0;
        B[i] = (double)rand() / (double)RAND_MAX * 2.0 - 1.0;
    }

    // Compute matrix product C = AB using mmul1 and compute time 
    tic = clock();
    mmul1(A, B, C1, n);
    toc = clock();
    elapsed_time = ((double) (toc - tic)) * 1000.0 / (double)CLOCKS_PER_SEC;
    printf("%.2f\n %.2f\n", elapsed_time, C1[n*n-1]);

    // Compute matrix product C = AB using mmul2 and compute time 
    tic = clock();
    mmul2(A, B, C2, n);
    toc = clock();
    elapsed_time = ((double) (toc - tic)) * 1000.0 / (double)CLOCKS_PER_SEC;
    printf("%.2f\n %.2f\n", elapsed_time, C2[n*n-1]);

    // Compute matrix product C = AB using mmul3 and compute time 
    double *Bt = (double*) malloc(n*n*sizeof(double));
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            Bt[j*n+i] = B[i*n+j];
        }
    }
    tic = clock();
    mmul3(A, Bt, C3, n);
    toc = clock();
    elapsed_time = ((double) (toc - tic)) * 1000.0 / (double)CLOCKS_PER_SEC;
    printf("%.2f\n %.2f\n", elapsed_time, C3[n*n-1]);
    

    // Compute matrix product C = AB using mmul4
    double *At = (double*) malloc(n*n*sizeof(double));
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            At[j*n+i] = A[i*n+j];
        }
    }
    tic = clock();
    mmul4(At, B, C4, n);
    toc = clock();
    elapsed_time = ((double) (toc - tic)) * 1000.0 / (double)CLOCKS_PER_SEC;
    printf("%.2f\n %.2f\n", elapsed_time, C4[n*n-1]);

    // Free memory
    free(A);
    free(B);
    free(C1);
    free(C2);
    free(C3);
    free(C4);
    free(Bt);
    free(At);

    return 0;
}
