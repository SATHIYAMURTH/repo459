#include "struct.h"
#include <stdio.h>
#include <stdlib.h>

int main()

{
 printf("Size of structure A: %zu bytes\n", sizeof(struct A));
 printf("Size of structure B: %zu bytes\n", sizeof(struct B));
 
 //Allocating memory for structure A 
 struct A *pA;
 pA = (struct A*)malloc(sizeof(struct A));
 
 //Set new values structure A
 pA->i = 44;
 pA->c = 'z';
 pA->d = 69.420;
 
 printf("Field 1 of struct A: %d\n", pA->i);
 printf("Field 2 of struct A: %c\n", pA->c);
 printf("Field 3 of struct A: %f\n", pA->d);
 
 free(pA);
 return 0;
 }
 