import numpy as np
import sys

# using loadtxt()

input = sys.argv[1]   # Part (a)- getting csv name from command line argument

arr = np.loadtxt(input,delimiter=",", dtype=str)
#print(arr)

# Print (b)- Frobenius norm
print(np.linalg.norm(arr, 'fro'))