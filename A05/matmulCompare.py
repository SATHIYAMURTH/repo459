from pyMatmul import *   # Part (a)
import numpy as np
import sys
import random
from time import perf_counter
import matplotlib.pyplot as plt

 N=[2**5, 2**6,2**7,2**8]


input = sys.argv[1]   # Part (b)
if input==0:
    plotBool=False
else:
    plotBool=True

time1=[]
time2=[]

# Normal List- Part (c)
for N in [2**5, 2**6, 2**7, 2**8]:
    print('\n N = ',N)
    A_list = [[random.uniform(-1, 1) for j in range(N)] for i in range(N)]
    B_list = [[random.uniform(-1, 1) for j in range(N)] for i in range(N)]

    # Numpy array
    A = np.array(A_list)
    B = np.array(B_list)

    #Part (d)
    t_start = perf_counter()
    result=matmul(A_list,B_list)
    t_stop = perf_counter()
    print("-------Matrix Multiplication of Lists------")
    time_taken=(t_stop-t_start)/1000
    time1.append(time_taken)
    print('Time in milliseconds for n=',N, ' is ', time_taken) 
   
    print("-------Numpy Multiplication------")
    t_start = perf_counter()
    result2=A@B
    t_stop = perf_counter()
    time_taken=(t_stop-t_start)/1000
    time2.append(time_taken)
    print('Time in milliseconds for n=',N, ' is ', time_taken)
    

# Part (e)
N=2**8

A_list = [[random.uniform(-1, 1) for j in range(N)] for i in range(N)]
B_list = [[random.uniform(-1, 1) for j in range(N)] for i in range(N)]

# Numpy array
A = np.array(A_list)
B = np.array(B_list)
t_start = perf_counter()
result=matmul(A_list,B_list)
t_stop = perf_counter()

#print("-------Matrix Multiplication of Lists------")
print('\n\n')
print(result[0][0])
print((t_stop-t_start)/1000)
   
#print("-------Numpy Multiplication------")
t_start = perf_counter()
result2=A@B
t_stop = perf_counter()
print(result2[0][0])
print((t_stop-t_start)/1000)



# Part (f)- Plotting loglog
if plotBool==True:
    N=[2**5, 2**6,2**7,2**8]
    plt.plot(N, time1, label = "Matrix multiplication with matmul()")
    plt.plot(N, time2, label = "A@B")
    plt.legend()
    plt.xscale('log',base=2) 
    plt.yscale('log',base=10) 
    plt.title('Time taken for matrix multiplication for different values of n')
    plt.xlabel('n (log 2)')
    plt.ylabel('time taken (ms) (log 10)')
    plt.show()




