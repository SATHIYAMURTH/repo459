from pySort import *   # Part (a)
import numpy as np
#import sys
import random
from time import perf_counter
import matplotlib.pyplot as plt
import argparse
import copy


# Part (b)
# Used argparse for the bonus
parser = argparse.ArgumentParser()
parser.add_argument(dest='argument1', help="This is the first argument")
args = parser.parse_args()
input=args.argument1


# Run file sorterCompare.py 0 <- this is the commmand line argument (0 or 1)
if input==True:
    plotBool=False
else:
    plotBool=True

#print(input)

time1=[]
time2=[]
time3=[]
#Part (c)
# Normal List- Part (c)
for N in [2**10, 2**11,2**12,2**13,2**14]:
    print('\nN = ',N)
    #Initializing required lists
    A_list = [random.randint(-10, 10) for j in range(N)]
    B_list=copy.deepcopy(A_list)  #deep copy
    # Numpy array
    A = np.array(A_list)

    #Part (d)- Sorting A_list
    t_start = perf_counter()
    result1=sorter(A_list)
    t_stop = perf_counter()
    print("-------Sorting A_list------")
    time_taken=(t_stop-t_start)*1000
    time1.append(time_taken)
    print('Time in milliseconds for n=',N, ' is ', time_taken)  
   
   # Sorting B_list
    print("-------Sorting B_list------")
    t_start = perf_counter()
    result2=B_list.sort()
    t_stop = perf_counter()
    time_taken=(t_stop-t_start)*1000
    time2.append(time_taken)
    print('Time in milliseconds for n=',N, ' is ', time_taken)

    #Sorting A
    print("-------Sorting A (numpy array)------")
    t_start = perf_counter()
    result3=np.sort(A)
    t_stop = perf_counter()
    time_taken=(t_stop-t_start)*1000
    time3.append(time_taken)
    print('Time in milliseconds for n=',N, ' is ', time_taken)

# Part (e)- Printing first element and time taken
N= 2**14
A_list = [random.randint(-10, 10) for j in range(N)]
B_list=copy.deepcopy(A_list)  #deep copy
# Numpy array
A = np.array(A_list)

#Sorting A_list
t_start = perf_counter()
result1=sorter(A_list)
t_stop = perf_counter()
time_taken=(t_stop-t_start)*1000
print(A_list[0])
print(time_taken)  
   
# Sorting B_list
t_start = perf_counter()
result2=B_list.sort()
t_stop = perf_counter()
time_taken=(t_stop-t_start)*1000
print(B_list[0])
print(time_taken)

#Sorting A
t_start = perf_counter()
result3=np.sort(A)
t_stop = perf_counter()
time_taken=(t_stop-t_start)*1000
print(A[0])
print(time_taken)




# Part (f)- Plotting log log
if plotBool==True:
    N=[2**10,2**11,2**12,2**13,2**14]
    plt.plot(N, time1, label = "A_list")
    plt.plot(N, time2, label = "B_list")
    plt.plot(N, time3, label='A')
    plt.legend()
    plt.xscale('log',base=2) 
    plt.yscale('log',base=10) 
    plt.title('Time taken for sorting lists for different values of n')
    plt.xlabel('n (log 2)')
    plt.ylabel('time taken (ms) (log 10)')
    plt.show()