from pyMatmul import matmul
from time import perf_counter
import random

# For n=7
X=[[random.random() for i in range(128)] for j in range(128)]
Y=[[random.random() for i in range(128)] for j in range(128)] 
t_start = perf_counter()
matmul(X,Y)
t_stop = perf_counter()
print('Time in seconds for n=7:',t_stop-t_start)

# For n=8
X=[[random.random() for i in range(256)] for j in range(256)]
Y=[[random.random() for i in range(256)] for j in range(256)] 
t_start = perf_counter()
matmul(X,Y)
t_stop = perf_counter()
print('Time in seconds for n=8:',t_stop-t_start)

# For n=9
X=[[random.random() for i in range(512)] for j in range(512)]
Y=[[random.random() for i in range(512)] for j in range(512)] 
t_start = perf_counter()
matmul(X,Y)
t_stop = perf_counter()
print('Time in seconds for n=9:',t_stop-t_start)