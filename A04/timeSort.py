from pySort import sorter
import random
from time import perf_counter

# for n=3
a=random.sample(range(9999),1000)
print(a[0])
t_start = perf_counter()
sorter(a)
t_stop = perf_counter()
print('Time in seconds for n=3:',t_stop-t_start)
print(a[0])

# for n=4
a=random.sample(range(99999),10000)
print(a[0])
t_start = perf_counter()
sorter(a)
t_stop = perf_counter()
print('Time in seconds for n=4:',t_stop-t_start)
print(a[0])

# for n=5
a=random.sample(range(999999),100000)
print(a[0])
t_start = perf_counter()
sorter(a)
t_stop = perf_counter()
print('Time in seconds for n=5:',t_stop-t_start)
print(a[0])

# for n=6
a=random.sample(range(9999999),1000000)
print(a[0])
t_start = perf_counter()
sorter(a)
t_stop = perf_counter()
print('Time in seconds for n=6:',t_stop-t_start)
print(a[0])