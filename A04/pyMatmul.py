def matmul(X,Y):
  result = [[0 for i in range(len(Y[0]))] for j in range(len(X))]
  for i in range(len(X)):
    for j in range(len(Y[0])):
        for k in range(len(Y)):
            result[i][j] += X[i][k] * Y[k][j]

  return result