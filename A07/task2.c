#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "mvmul.h"

int main(int argc, char *argv[]) {
    // parse command line argument
    int n = atoi(argv[1]);

    // dynamically allocate matrix A as a one-dimensional array
    double *A = (double *) malloc(n * n * sizeof(double));

 
    double *b = (double *) malloc(n * sizeof(double));
    double *c = (double *) malloc(n * sizeof(double));

    
    srand(time(NULL));
    for (int i = 0; i < n * n; i++) {
        A[i] = (double) rand() / RAND_MAX * 2 - 1;
    }

    
    for (int i = 0; i < n; i++) {
        b[i] = 1.0;
    }

    // perform matrix-vector multiplication and measure time
    clock_t start = clock();
    mvmul(A, b, c, n);
    clock_t end = clock();
    double time_spent = (double)(end - start) / CLOCKS_PER_SEC * 1000;

    // print last entry of c
    printf("%lf\n", c[n - 1]);

    // free dynamically allocated memory
    free(A);
    free(b);
    free(c);

    // print time taken for multiplication
    printf("Time taken: %lf ms\n", time_spent);

    return 0;
}
