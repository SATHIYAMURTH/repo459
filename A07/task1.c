#include <stdio.h>
#include "sort.h"

int main() {
    int A[] = {4, 3, 2, 1, 44, 5, 18, 19, 20, 38, 46, 55};
    size_t n = sizeof(A) / sizeof(A[0]);

    printf("Original array: ");
    for (size_t i = 0; i < n; i++) {
        printf("%d ", A[i]);
    }
    printf("\n");

    sort(A, n);

    printf("Sorted array: ");
    for (size_t i = 0; i < n; i++) {
        printf("%d ", A[i]);
    }
    printf("\n");

    return 0;
}
