import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint

# Define the circuit parameters
R = 10.0  # resistance in ohms
L = 1.0   # inductance in henrys
C = 0.1   # capacitance in farads

# Define the state space model
A = np.array([[0, 1], [-1/(L*C), -R/L]])
B = np.array([[0], [1/(L*C)]])
C = np.array([1, 0])
D = 0
sys = (A, B, C, D)

# Define the transfer function
num = [1/(L*C), 0]
den = [1, R/L, 1/(L*C)]
s = np.linspace(-100, 100, 1000)
w = 2*np.pi*s
H = np.polyval(num, w)/(np.polyval(den, w)*1j)

# Plot the frequency response
plt.figure()
plt.plot(s, 20*np.log10(np.abs(H)))
plt.xlabel('Frequency (rad/s)')
plt.ylabel('Magnitude (dB)')
plt.title('Frequency Response of RLC Circuit')
plt.grid(True)

# Compute and plot the step response
t = np.linspace(0, 10, 1000)
u = np.ones_like(t)
sys_ss = control.TransferFunction(num, den).to_ss()
x0 = [0, 0]
t, y, x = control.forced_response(sys_ss, t, u, X0=x0)

plt.figure()
plt.plot(t, y)
plt.xlabel('Time (s)')
plt.ylabel('Output')
plt.title('Step Response of RLC Circuit')
plt.grid(True)

# Compute and plot the phase portrait
x1 = np.linspace(-1, 1, 20)
x2 = np.linspace(-1, 1, 20)
X1, X2 = np.meshgrid(x1, x2)
X1dot = X2
X2dot = -(R/L)*X2 - (1/(L*C))*X1
plt.figure()
plt.quiver(X1, X2, X1dot, X2dot)
plt.xlabel('State variable x1')
plt.ylabel('State variable x2')
plt.title('Phase Portrait of RLC Circuit')
plt.grid(True)

plt.show()
