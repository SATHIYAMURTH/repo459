
from os import linesep
import schemdraw
import schemdraw.elements as elm

import numpy as np
from numpy import linspace,cos,sin,arccos,pi,exp,heaviside,fft,angle,log,sqrt
import matplotlib.pyplot as plt
from matplotlib import animation,rc
rc('animation', html='jshtml')

# draw circuit diagram
def drawCircuitDiagram():
    with schemdraw.Drawing() as d:
        V1 = d.add(elm.SourceSin().down().length(d.unit*1.5).flip())
        L = d.add(elm.Inductor2().right(3).at(V1.start))
        d += elm.Resistor()
        d += elm.CurrentLabelInline('in').at(L).color('r')
        d += elm.SourceSin().down(4.5).flip()
        d += elm.Line().left().length(d.unit*2)

    fig, ax = plt.subplots(figsize=(6, 6))  # plot figure
    d.draw()
    ax.axis('off')
    ax.text(.5, -0.5, r'$i(t)$', size=24, color='r')
    ax.text(1.5, 0.5, r'$L$', size=24)
    ax.text(4.5, 0.4, r'$R$', size=24)
    ax.text(0.5, -3, '+         -', size=24, rotation=-90, color='b')
    ax.text(0.5, -2.5, r'$v_1(t)$', size=24, color='b')
    ax.text(6.5, -3, '+         -', size=24, rotation=-90, color='g')
    ax.text(6.5, -2.5, r'$v_2(t)$', size=24, color='g')
    return fig, ax




w = 2*pi*60
Pbase = 5000 # base power
Vbase = 120 # base voltage in rms
Ibase = Pbase/Vbase # base current in rms
Zbase = Vbase/Ibase # base impedance in ohm
X_pu = 0.6 # reactance of the inductance in per unit
X = X_pu * Zbase # ractance in ohm
L = X/w # inductance in Henry
tau = 1.5*(2*pi/w) # time constant of the RL circuit, 3 times the ac period
R = L/tau
R_pu = R/Zbase
V2pu = 1.0 #
Ipu = 1.0 # current magnitude before t0 
theta_I = 0
I_pu = V2pu/abs(R_pu + 1j*X_pu) # steady state current magnitude after t0
theta_Iss = pi - angle(R_pu + 1j*X_pu)
V1pu = abs(V2pu + Ipu*exp(1j*theta_I)*(R_pu + 1j*X_pu))
theta_1 = angle(V2pu + Ipu*exp(1j*theta_I)*(R_pu + 1j*X_pu))

def v1(t,t0):
  return V1pu*cos(w*t+theta_1)*heaviside(t0-t,0)

def iss(t,t0):
  return Ipu*cos(w*t + theta_I)*heaviside(t0 - t,1) + I_pu*cos(w*t + theta_Iss)*heaviside(t-t0, 0)

def itr(t,t0):
  Itr = Ipu*cos(w*t0 + theta_I) - I_pu*cos(w*t0 + theta_Iss)
  return Itr*exp(-(t-t0)/tau)*heaviside(t-t0,0)

def ii(t,t0):
  return iss(t,t0) + itr(t,t0)

Nppc = 1024 # number of points per cycle
Nc = 10 # number of cycles
Npt = Nc*Nppc # total number of points
T_stop = Nc*2*pi/w # duration of the plot
t = linspace(0,T_stop,Npt)

Nf1 =30 # number of waveforms
Nf2 = 30 # number of animations per waveform
Nf = Nf1*Nf2
t0 = linspace(T_stop*0.2,T_stop*0.2+2*pi/w,Nf1)

v2t = V2pu*cos(w*t)



fig = plt.figure()
plt.subplots_adjust(left=0.1, right=0.9, bottom=0.1, top=0.9, wspace=0.2, hspace=0.2)
ax = plt.axes(xlim=(-0.1, 5), ylim=(-2.5, 2.65))
ax2 = fig.add_axes([0.55, 0.6, 0.4, 0.275]) # drawing area for circuit diagram
ax3 = fig.add_axes([0.1, 0.6, 0.4, 0.35]) # drawing area for phasor diagram

fig.tight_layout()
ax.axis('off')
ax2.axis('off')
ax3.axis('off')

x0 = [0,0,2.5] # orgins of x-axis
y0 = [1.25,-1.25,-1.25] # origins of y-axis
xln = [2.45, 2.45, 2.45] # length of x-axis
yln = [2.45, 2.45, 2.45] # length of y-axis

# plot x,y axes
ax.arrow(x0[0], y0[0], xln[0], 0, width=0.002, head_width=0.04, ec='k', fc='k', length_includes_head=True)
ax.arrow(x0[0], y0[0]-0.5*yln[0], 0, yln[0], width=0.002, head_width=0.04, ec='k', fc='k', length_includes_head=True)
ax.arrow(x0[1],y0[1], xln[1], 0, width=0.002, head_width=0.04, ec='k', fc='k', length_includes_head=True)
ax.arrow(x0[1], y0[1]-0.5*yln[1], 0, yln[1], width=0.002, head_width=0.04, ec='k', fc='k', length_includes_head=True)
ax.arrow(x0[2], y0[2], xln[2], 0, width=0.002, head_width=0.04, ec='k', fc='k', length_includes_head=True)
ax.arrow(x0[2], y0[2]-0.5*yln[2], 0, yln[2], width=0.002, head_width=0.04, ec='k', fc='k', length_includes_head=True)


ax.text(x0[0]+0.05, y0[0]+yln[0]*0.5, r'$v_1(t)$,', size = 20, color='b')
ax.text(x0[0]+0.35, y0[0]+yln[0]*0.5, r'$v_2(t)$', size = 20, color='g')
ax.text(x0[0]+xln[0]-0.1, y0[0] - 0.1, r'$t$', size = 20)
ax.text(x0[1]+0.05, y0[1]+yln[1]*0.5, r'$i(t)$', size = 20, color='r')
ax.text(x0[1]+xln[1]-0.1, y0[1]-0.1, r'$t$',size = 20)
ax.text(x0[2]+0.05,y0[2]+yln[1]*.5,r'$i_{ss}(t)$,',size = 20, color='m')
ax.text(x0[2]+0.35,y0[2]+yln[1]*.5,r'$i_{tr}(t)$',size = 20, color='c')
ax.text(x0[2]+xln[2]-0.1, y0[2]-0.1, r'$t$',size = 20)



fig2, ax2 = drawCircuitDiagram()

line_v1, = ax.plot([], [], '-b', lw=2)
line_v2, = ax.plot([], [], '-g', lw=2)
line_i, = ax.plot([], [], '-r', lw=2)
line_iss, = ax.plot([], [], '-m', lw=2)
line_itr, = ax.plot([], [], '-c', lw=2)


def init():
  line_v1.set_data([],[])
  line_v2.set_data([],[])
  line_i.set_data([],[])
  line_iss.set_data([],[])
  line_itr.set_data([],[])
  return line_v1,line_v2,line_i,line_iss,line_itr,

# animation function.  This is called sequentially
def animate_func(i):
  t00 = t0[int((i-1)/Nf2)]
  nn = (i%Nf2)*int(Npt/Nf2)
  v1t = v1(t,t00)
  it = ii(t,t00)
  isst = iss(t,t00)
  itrt = itr(t,t00)
  line_v1.set_data(t[0:nn]/T_stop*xln[0]+x0[0],y0[0] + v1t[0:nn]*yln[0]*0.4)
  line_v2.set_data(t[0:nn]/T_stop*xln[0]+x0[0],y0[0] + v2t[0:nn]*yln[0]*0.4)
  line_i.set_data(t[0:nn]/T_stop*xln[1]+x0[1],y0[1] + it[0:nn]*yln[1]*0.2)
  line_iss.set_data(t[0:nn]/T_stop*xln[2]+x0[2],y0[2] + isst[0:nn]*yln[2]*0.2)
  line_itr.set_data(t[0:nn]/T_stop*xln[2]+x0[2],y0[2] + itrt[0:nn]*yln[2]*0.2)
  return line_v1,line_v2,line_i,line_iss,line_itr,

anim = animation.FuncAnimation(fig, animate_func, init_func=init, frames=Nf, interval=50)

fn = r"SinglePhase_SC_v1.mp4" 
writervideo = animation.FFMpegWriter(fps=30) 
anim.save(fn, writer=writervideo,dpi = 200)