from os import linesep
import schemdraw
import schemdraw.elements as elm

import numpy as np
from numpy import linspace,cos,sin,arccos,pi,exp,heaviside,fft,angle,log,sqrt
import matplotlib.pyplot as plt
from matplotlib import animation,rc
rc('animation', html='jshtml')

# function to draw the circuit diagram
def drawCircuitDiagram(ax,asc):
  if asc == 1:
    v1Color = 'white'
    shortLineColor='black'
  else:
    v1Color = 'black'
    shortLineColor = 'white'
  with schemdraw.Drawing(show=False) as d:
    #phase - a
    Va1 = d.add(elm.SourceSin(color=v1Color).right().length(d.unit*1))
    d += elm.Line().right(0.5)
    L = d.add(elm.Inductor2())
    d += elm.Resistor()
    d += elm.CurrentLabelInline('in').at(L).color('r')
    Va2 = d.add(elm.SourceSin().right().length(d.unit*1))
    #phase - b
    d += elm.Line(color=v1Color).down(4).at(Va1.start)
    Vbase1 = d.add(elm.SourceSin(color=v1Color).right().length(d.unit*1))
    d += elm.Line().right(0.5)
    L = d.add(elm.Inductor2())
    d += elm.Resistor()
    d += elm.CurrentLabelInline('in').at(L).color('b')
    Vbase2 = d.add(elm.SourceSin().right().length(d.unit*1))
    #phase - c
    d += elm.Line(color=v1Color).down(4).at(Vbase1.start)
    Vc1 = d.add(elm.SourceSin(color=v1Color).right().length(d.unit*1.))
    d += elm.Line().right(0.5)
    L = d.add(elm.Inductor2())
    d += elm.Resistor()
    d += elm.CurrentLabelInline('in').at(L).color('g')
    Vc2 = d.add(elm.SourceSin().right().length(d.unit*1))
    d += elm.Line().down(8).at(Va2.end)

   
    d += elm.Line(color=shortLineColor).down(8).at(Va1.end)

    ax.clear()
    ax.plot([0, 1, 2, 3, 4], [0, 1, 0, -1, 0], 'r-', linewidth=2)
    ax.set_xlim(0, 4)
    ax.set_ylim(-1.5, 1.5)
    ax.axis('off')


    if asc == 0:
        ax.text(1, 0.5, 'asc=0', size=12, color='black')
    else:
        ax.text(1, 0.5, 'asc=1', size=12, color='black')

    # Rest of your code

  return


w = 2*pi*60
Pbase = 5000 # base power
Vbase = 120 # base voltage in rms
Ib = Pbase/Vbase # base current in rms
Zb = Vbase/Ib # base impedance in ohm
X_pu = 0.6 # reactance of the inductance in per unit
X = X_pu * Zb # ractance in ohm
L = X/w # inductance in Henry
tau = 1.5*(2*pi/w) # time constant of the RL circuit
R = L/tau
R_pu = R/Zb
V2pu = 1.0 #
I_pu = 1.0 # current magnitude before t0 
theta_I = 0
Iss_pu = V2pu/abs(R_pu + 1j*X_pu) # steady state current magnitude after t0
theta_Iss = pi - angle(R_pu + 1j*X_pu)
V1pu = abs(V2pu + I_pu*exp(1j*theta_I)*(R_pu + 1j*X_pu))
theta1 = angle(V2pu + I_pu*exp(1j*theta_I)*(R_pu + 1j*X_pu))

def v1(t,t0,n):
  #n = 0,1,2 for phase a,b,c, respectively
  return V1pu*cos(w*t + theta1 - n*2*pi/3)*heaviside(t0-t,0)

def v2(t,n):
  #n = 0,1,2 for phase a,b,c, respectively
  return V2pu*cos(w*t - n*2*pi/3)

def v(t,t0,n):
  return v1(t,t0,n) - v2(t,n)

def iss(t,t0,n):
  #n = 0,1,2 for phase a,b,c, respectively
  return I_pu*cos(w*t + theta_I - n*2*pi/3)*heaviside(t0 - t,1) + Iss_pu*cos(w*t + theta_Iss - n*2*pi/3)*heaviside(t-t0, 0)

def itr(t,t0,n):
  Itr = I_pu*cos(w*t0 + theta_I - n*2*pi/3) - Iss_pu*cos(w*t0 + theta_Iss - n*2*pi/3)
  return Itr*exp(-(t-t0)/tau)*heaviside(t-t0,0)

def ii(t,t0,n):
  return iss(t,t0,n) + itr(t,t0,n)

Nppc = 1024 # number of points per cycle
Nc = 7 # number of cycles
Npt = Nc*Nppc # total number of points
T_stop = Nc*2*pi/w # duration of the plot
t = linspace(0,T_stop,Npt)

Nf1 =20 # number of waveforms
Nf2 = 40 # number of animations per waveform
Nf = Nf1*Nf2
t0 = linspace(T_stop*0.2,T_stop*0.2+2*pi/w,Nf1)


fig = plt.figure(figsize=(15,15))
ax = plt.axes(xlim=(-0.1, 5), ylim=(-2.5, 2.65))
ax2 = fig.add_axes([0.55, 0.6, 0.4, 0.275]) # drawing area for circuit diagram
ax3 = fig.add_axes([0.1, 0.6, 0.4, 0.35]) # drawing area for phasor diagram

fig.tight_layout()
ax.axis('off')
ax2.axis('off')
ax3.axis('off')

x0 = [0,0,2.5] # orgins of x-axis
y0 = [1.25,-1.25,-1.25] # origins of y-axis
xln = [2.45, 2.45, 2.45] # length of x-axis
yln = [2.45, 2.45, 2.45] # length of y-axis

# plot x,y axes
ax.arrow(x0[0], y0[0], xln[0], 0, width=0.002, head_width=0.04, ec='k', fc='k', length_includes_head=True)
ax.arrow(x0[0], y0[0]-0.5*yln[0], 0, yln[0], width=0.002, head_width=0.04, ec='k', fc='k', length_includes_head=True)
ax.arrow(x0[1],y0[1], xln[1], 0, width=0.002, head_width=0.04, ec='k', fc='k', length_includes_head=True)
ax.arrow(x0[1], y0[1]-0.5*yln[1], 0, yln[1], width=0.002, head_width=0.04, ec='k', fc='k', length_includes_head=True)
ax.arrow(x0[2], y0[2], xln[2], 0, width=0.002, head_width=0.04, ec='k', fc='k', length_includes_head=True)
ax.arrow(x0[2], y0[2]-0.5*yln[2], 0, yln[2], width=0.002, head_width=0.04, ec='k', fc='k', length_includes_head=True)


ax.text(x0[0]+0.05, y0[0]+yln[0]*0.5, r'$v_a(t)$,', size = 20, color='r')
ax.text(x0[0]+0.30, y0[0]+yln[0]*0.5, r'$v_b(t),$', size = 20, color='b')
ax.text(x0[0]+0.55, y0[0]+yln[0]*0.5, r'$v_c(t)$', size = 20, color='g')
ax.text(x0[0]+xln[0]-0.1, y0[0] - 0.1, r'$t$', size = 20)
ax.text(x0[1]+0.05, y0[1]+yln[1]*0.5, r'$i_a(t),$', size = 20, color='r')
ax.text(x0[1]+0.30, y0[1]+yln[1]*0.5, r'$i_b(t),$', size = 20, color='b')
ax.text(x0[1]+0.55, y0[1]+yln[1]*0.5, r'$i_c(t)$', size = 20, color='g')
ax.text(x0[1]+xln[1]-0.1, y0[1]-0.1, r'$t$',size = 20)
ax.text(x0[2]+0.05, y0[2]+yln[2]*0.5, r'$i_{a\_tr}(t),$', size = 20, color='r')
ax.text(x0[2]+0.40, y0[2]+yln[2]*0.5, r'$i_{b\_tr}(t),$', size = 20, color='b')
ax.text(x0[2]+0.75, y0[2]+yln[2]*0.5, r'$i_{c\_tr}(t)$', size = 20, color='g')
ax.text(x0[2]+xln[2]-0.1, y0[2]-0.1, r'$t$',size = 20)


line_va, = ax.plot([], [], '-r', lw=2)
line_Vbase, = ax.plot([], [], '-b', lw=2)
line_vc, = ax.plot([], [], '-g', lw=2)

line_ia, = ax.plot([], [], '-r', lw=2)
line_ib, = ax.plot([], [], '-b', lw=2)
line_ic, = ax.plot([], [], '-g', lw=2)

line_ia_tr, = ax.plot([], [], '-r', lw=2)
line_ib_tr, = ax.plot([], [], '-b', lw=2)
line_ic_tr, = ax.plot([], [], '-g', lw=2)

def init():
  line_va.set_data([],[])
  line_Vbase.set_data([],[])
  line_vc.set_data([],[])
  line_ia.set_data([],[])
  line_ib.set_data([],[])
  line_ic.set_data([],[])
  line_ia_tr.set_data([],[])
  line_ib_tr.set_data([],[])
  line_ic_tr.set_data([],[])
  return line_va,line_Vbase,line_vc,line_ia,line_ib,line_ic,line_ia_tr,line_ib_tr,line_ic_tr,


def animate(i):
  t00 = t0[int((i-1)/Nf2)]
  nn = (i%Nf2)*int(Npt/Nf2)
  if t[nn-1] < t00:
    drawCircuitDiagram(ax2,0)
  else:
    drawCircuitDiagram(ax2,1)


  vat = v(t,t00,0)
  Vbaset = v(t,t00,1)
  vct = v(t,t00,2)
  iat = ii(t,t00,0)
  ibt = ii(t,t00,1)
  ict = ii(t,t00,2)
  
  iatrt = itr(t,t00,0)
  ibtrt = itr(t,t00,1)
  ictrt = itr(t,t00,2)
  line_va.set_data(t[0:nn]/T_stop*xln[0]+x0[0],y0[0] + vat[0:nn]*yln[0]*0.4)
  line_Vbase.set_data(t[0:nn]/T_stop*xln[0]+x0[0],y0[0] + Vbaset[0:nn]*yln[0]*0.4)
  line_vc.set_data(t[0:nn]/T_stop*xln[0]+x0[0],y0[0] + vct[0:nn]*yln[0]*0.4)
  line_ia.set_data(t[0:nn]/T_stop*xln[1]+x0[1],y0[1] + iat[0:nn]*yln[1]*0.2)
  line_ib.set_data(t[0:nn]/T_stop*xln[1]+x0[1],y0[1] + ibt[0:nn]*yln[1]*0.2)
  line_ic.set_data(t[0:nn]/T_stop*xln[1]+x0[1],y0[1] + ict[0:nn]*yln[1]*0.2)
  line_ia_tr.set_data(t[0:nn]/T_stop*xln[2]+x0[2],y0[2] + iatrt[0:nn]*yln[2]*0.2)
  line_ib_tr.set_data(t[0:nn]/T_stop*xln[2]+x0[2],y0[2] + ibtrt[0:nn]*yln[2]*0.2)
  line_ic_tr.set_data(t[0:nn]/T_stop*xln[2]+x0[2],y0[2] + ictrt[0:nn]*yln[2]*0.2)
  return line_va,line_Vbase,line_vc,line_ia,line_ib,line_ic,line_ia_tr,line_ib_tr,line_ic_tr,

anim = animation.FuncAnimation(fig, animate, init_func=init, frames=Nf, interval=50)

fn = r"ThreeePhase_ASC.mp4" 
writervideo = animation.FFMpegWriter(fps=20) 
anim.save(fn, writer=writervideo,dpi = 200)