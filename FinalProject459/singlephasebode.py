import numpy as np
import matplotlib.pyplot as plt
from scipy import signal

# Define the RL circuit parameters
R = 1  # Resistance (Ohms)
L = 1  # Inductance (Henries)

# Define the transfer function for the series RL circuit
num1 = [0, 1]
den1 = [L, R]

# Define the transfer function for the parallel RL circuit
num2 = [0, 1/L]
den2 = [1, R/L]

# Create the transfer function models
sys1 = signal.TransferFunction(num1, den1)
sys2 = signal.TransferFunction(num2, den2)

# Time vector
t = np.linspace(0, 5, 1000)

# Step input
u = np.ones_like(t)

# Simulate the response of the series RL circuit
t1, y1, _ = signal.lsim(sys1, U=u, T=t)

# Simulate the response of the parallel RL circuit
t2, y2, _ = signal.lsim(sys2, U=u, T=t)

# Plot the transient response
fig, (ax1, ax2) = plt.subplots(2, 1, figsize=(8, 8))

ax1.plot(t1, y1, label='Voltage')
ax1.set_xlabel('Time')
ax1.set_ylabel('Voltage')
ax1.set_title('Series RL Circuit')
ax1.legend()
ax1.grid(True)

ax2.plot(t2, y2, label='Voltage')
ax2.set_xlabel('Time')
ax2.set_ylabel('Voltage')
ax2.set_title('Parallel RL Circuit')
ax2.legend()
ax2.grid(True)

plt.tight_layout()
plt.show()

# Calculate and plot the Bode plot
w1, mag1, phase1 = signal.bode(sys1)
w2, mag2, phase2 = signal.bode(sys2)

fig, (ax3, ax4) = plt.subplots(2, 1, figsize=(8, 8))

ax3.semilogx(w1, mag1)
ax3.set_xlabel('Frequency')
ax3.set_ylabel('Magnitude (dB)')
ax3.set_title('Bode Plot: Series RL Circuit')
ax3.grid(True)

ax4.semilogx(w2, mag2)
ax4.set_xlabel('Frequency')
ax4.set_ylabel('Magnitude (dB)')
ax4.set_title('Bode Plot: Parallel RL Circuit')
ax4.grid(True)

plt.tight_layout()
plt.show()

