import numpy as np
from scipy.integrate import solve_ivp
import matplotlib.pyplot as plt
from scipy import signal

# Define the circuit parameters
R = 10  # Resistance (ohms)
L = 1e-3  # Inductance (H)
f = 50  # Frequency (Hz)
V_phase = 230  # Phase voltage (V)
N_phases = 3  # Number of phases

# Define the state space representation
A = np.array([[-R/L]])
A = np.tile(A, (N_phases, N_phases))
B = np.array([[1/L]])
B = np.tile(B, (N_phases, 1))
C = np.eye(N_phases)  # Output is the currents of all phases
D = np.zeros((N_phases, 1))

# Create the transfer function model
sys = signal.StateSpace(A, B, C, D)

# Define the time span
t_start = 0  # Start time (s)
t_end = 0.2  # End time (s)
t_span = (t_start, t_end)  # Time span

# Define the input function (step input)
def input_func(t):
    if t < 0.01:
        return 0
    else:
        return V_phase

# Define the state derivative function
def state_derivative(t, x):
    u = input_func(t)
    return np.dot(A, x) + np.dot(B, np.array([[u]]))

# Solve the state equations
sol = solve_ivp(state_derivative, t_span, [0] * N_phases, method='BDF', vectorized=True)

# Extract the state variables
t = sol.t
x = sol.y.T

# Calculate the phase currents
I = np.dot(C, x.T).T

# Plot the transient response of phase currents
plt.figure(figsize=(10, 6))
for i in range(N_phases):
    plt.plot(t, I[:, i], label='Phase {}'.format(i + 1))

plt.xlabel('Time (s)')
plt.ylabel('Current (A)')
plt.title('Transient Response of Phase Currents')
plt.legend()
plt.grid(True)
plt.show()