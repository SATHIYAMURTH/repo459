#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void test1(double* data, int elems, int stride){
    int i;
    double result = 0.0;
    volatile double sink;

    for (i = 0; i < elems; i += stride) {
        result += data[i];
    }
    /* Force compiler to not optimize away the loop */
    sink = result;
}

void test2(double* data, int elems, int stride){
    int i;
    volatile double result = 0.0;
    volatile double sink;

    for (i = 0; i < elems; i += stride) {
        result += data[i];
    }
    /* Force compiler to not optimize away the loop */
    sink = result;
}

int main() {
    // Initialize variables
    const int n_elems = 65536; // 512KB
    double* data = malloc(sizeof(double) * n_elems);
    srand(time(NULL)); 

    // Fill data with random double numbers in the range [-1,1]
    for (int i = 0; i < n_elems; i++) {
        data[i] = ((double)rand() / RAND_MAX) * 2.0 - 1.0;
    }

    // cache warm up
    test1(data, n_elems, 1);

    int strides[] = {1, 2, 4, 8, 11, 15, 17};
    // Run test2 for different stride values and get average time
    printf("timing for test 1 \n");

    for (int i = 0; i < sizeof(strides) / sizeof(int); i++) {
        double avg_time = 0.0;
        int num_iters = 100;
        for (int j = 0; j < num_iters; j++) {
            clock_t start_time = clock();
            test1(data, n_elems, strides[i]);
            clock_t end_time = clock();
            avg_time += (double)(end_time - start_time) / (double)CLOCKS_PER_SEC * 1000.0;
        }
        avg_time /= (double)num_iters;
        printf("%.2f ", avg_time);
    }
    printf("\n");

    printf("timing for test 2 \n");
    // Run test2 for different stride values and get average time
    for (int i = 0; i < sizeof(strides) / sizeof(int); i++) {
        double avg_time = 0.0;
        int num_iters = 100;
        for (int j = 0; j < num_iters; j++) {
            clock_t start_time = clock();
            test2(data, n_elems, strides[i]);
            clock_t end_time = clock();
            avg_time += (double)(end_time - start_time) / (double)CLOCKS_PER_SEC * 1000.0;
        }
        avg_time /= (double)num_iters;
        printf("%.2f ", avg_time);
    }
    printf("\n");

    // Clean up
    free(data);
    return 0;
}
