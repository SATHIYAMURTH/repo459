#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "matmul.h"


int main(int argc, char* argv[]) {
  if (argc != 2) {
    printf("Usage: %s n\n", argv[0]);
    return 1;
  }
  const size_t n = atoi(argv[1]);

  double *A = malloc(n * n * sizeof(double));
  double *B = malloc(n * n * sizeof(double));
  double *C = malloc(n * n * sizeof(double));

  srand(time(NULL));

  for (size_t i = 0; i < n * n; ++i) {
    A[i] = ((double)rand() / RAND_MAX) * 2.0 - 1.0;
    B[i] = ((double)rand() / RAND_MAX) * 2.0 - 1.0;
  }

  clock_t start = clock();
  mmul1(A, B, C, n);
  clock_t end = clock();

  double elapsed_time = (double)(end - start) * 1000.0 / CLOCKS_PER_SEC;

  printf("%.1f\n", elapsed_time);
  printf("%.3f\n", C[n*n-1]);

  free(A);
  free(B);
  free(C);


  return 0;
}

