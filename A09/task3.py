import random
import time

n = 3400
m = 1400
A = [[random.uniform(-1, 1) for j in range(m)] for i in range(n)]
B = [[random.uniform(-1, 1) for j in range(m)] for i in range(n)]

print(f"{A[-1][-1]}, {B[-1][-1]}")

start_time = time.time()
for i in range(n):
    for j in range(m):
        tmp = A[i][j]
        A[i][j] = B[i][j]
        B[i][j] = tmp
stop_time = time.time()
row_swap_time = (stop_time - start_time) * 1000

print(f"{A[-1][-1]}, {B[-1][-1]}")
print(f"{row_swap_time:.1f}")

start_time = time.time()
for j in range(m):
    for i in range(n):
        tmp = A[i][j]
        A[i][j] = B[i][j]
        B[i][j] = tmp
stop_time = time.time()
col_swap_time = (stop_time - start_time) * 1000

print(f"{A[-1][-1]}, {B[-1][-1]}")
print(f"{col_swap_time:.1f}")